package controledefornecedor.controller;

import controledefornecedor.modell.Produto;
import java.util.ArrayList;

public class ControleProduto {
    private ArrayList<Produto> listaProdutos;
    
    public ControleProduto(){
        listaProdutos = new ArrayList<Produto>();
    }
    
    public String vendaProduto(Produto umProduto){
        String mensagem = "O produto "+umProduto.getNome()+" foi vendido por R$"+umProduto.getValorVenda()+"!";
        return mensagem;
    }
    
    public String compraProduto(Produto umProduto){
        String mensagem = "O produto "+umProduto.getNome()+" foi comprado por R$"+umProduto.getValorCompra()+"!";
        return mensagem;
    }
    
    public void listaProdutos(){
        for(Produto umProduto: listaProdutos){
            System.out.println(umProduto.getNome());
        }
    }
    
}
