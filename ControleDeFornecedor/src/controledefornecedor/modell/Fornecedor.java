package controledefornecedor.modell;

import controledefornecedor.modell.Endereco;
import controledefornecedor.modell.Produto;
import java.util.ArrayList;

public class Fornecedor {
    private String nome;
    private String telefone;
    private Endereco endereco;
    private ArrayList<Produto> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
}